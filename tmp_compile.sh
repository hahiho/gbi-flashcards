# Compiles the flashcard with the given path to PDF,
# adding the necessary frontmatter beforehand.

if [ $# -eq 0 ]
then
	echo "USAGE: ./tmp_compile.sh <flashcard_path>"
	exit 1
fi

if [ ! -e "$1" ]
then
	echo "File doesn't exist: $1"
	exit 1
fi

meta1=`head -n 1 "$1" | tr -d '\n'`
meta2=`sed '2!d' "$1" | tr -d '\n'`
body=`tail -n +3 "$1"`
cat fm.tex > tmp_compile.tex
printf '\\begin{document}\n\\begin{flashcard}[%s]{%s}\n%s\n\\end{flashcard}\n\\end{document}\n' "$meta1" "$meta2" "$body" >> tmp_compile.tex
pdflatex tmp_compile.tex
