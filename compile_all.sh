# Compiles all flashcards to PDF

cat fm.tex > complete.tex
printf '\\begin{document}\n' >> complete.tex
for i in `ls flashcards`
do
	meta1=`head -n 1 "flashcards/$i" | tr -d '\n'`
	meta2=`sed '2!d' "flashcards/$i" | tr -d '\n'`
	body=`tail -n +3 "flashcards/$i"`
	printf '\\begin{flashcard}[%s]{%s}\n%s\n\\end{flashcard}\n' "$meta1" "$meta2" "$body" >> complete.tex
done
printf '\\end{document}\n' >> complete.tex
pdflatex complete.tex
